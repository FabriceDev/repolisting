//
//  ListItemsViewModel.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import Foundation
import Combine

class ListItemsViewModel: ObservableObject, Identifiable {
    
    @Published var list = [ItemsViewModel]()
    
    private let itemsService: ItemsServiceProtocol
    private unowned let coordinator: CoordinatorObject
    
    private var cancellable: AnyCancellable?
    
    init(coordinator: CoordinatorObject, itemsService: ItemsServiceProtocol = ItemsService()) {
        self.coordinator = coordinator
        self.itemsService = itemsService
    }
    
    func start(viewModel: ItemsViewModel) {
        coordinator.start(viewModel: viewModel)
    }
    
    func fetchingData() {
        do {
            cancellable = try itemsService.fetchListItems().sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    ()
                case.failure(let error):
                    print(error.localizedDescription)
                }
            }, receiveValue: { listItemsFromApi in
                self.list = listItemsFromApi.items.map({ ItemsViewModel(item: $0 )})
            })
        } catch {
            print(error.localizedDescription)
        }
    }
}
