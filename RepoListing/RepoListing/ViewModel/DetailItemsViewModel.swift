//
//  DetailItemsViewModel.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import Foundation

class DetailItemsViewModel: ObservableObject, Identifiable {
    
    @Published var viewModel: ItemsViewModel
    private unowned let coordinator: CoordinatorObject
    
    init(viewModel: ItemsViewModel, coordinator: CoordinatorObject) {
        self.viewModel = viewModel
        self.coordinator = coordinator
    }
}
