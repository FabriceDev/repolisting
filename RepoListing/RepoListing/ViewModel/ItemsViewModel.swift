//
//  ItemsViewModel.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import Foundation
import UIKit

struct ItemsViewModel: Identifiable {
    
    internal let id = UUID()
    
    private let item: Item
    
    var name: String {
        return item.name
    }
    
    var fullName: String {
        return item.fullName
    }
    
    var itemPrivate: Bool {
        return item.itemPrivate
    }
    
    var owner: Owner {
        return item.owner
    }
    
    var htmlURL: String {
        return item.htmlURL
    }
    
    var itemDescription: String? {
        return item.itemDescription
    }
    
    var createdAt: String {
        guard let date = item.createdAt.stringToDate() else { return "" }
        return "Created: \(date.dateToString())"
    }
    
    var updatedAt: String {
        guard let date = item.updatedAt.stringToDate() else { return "" }
        return "Last update: \(date.dateToString())"
    }
    
    var stargazersCount: Int {
        return item.stargazersCount
    }
    
    var license: License? {
        return item.license
    }
    
    var topics: [String] {
        return item.topics
    }
    
    var forks: Int {
        return item.forks
    }
    
    var openIssues: Int {
        return item.openIssues
    }
    
    var watchers: Int {
        return item.watchers
    }
    
    var score: Int {
        return item.score
    }
    
    init(item: Item) {
        self.item = item
    }
}
