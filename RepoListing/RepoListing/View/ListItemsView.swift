//
//  ListItemsView.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import SwiftUI

struct ListItemsView: View {
    
    @ObservedObject var listItemsVM: ListItemsViewModel
    
    var body: some View {
        List(listItemsVM.list, id: \.id) { item in
            ItemsCell(item: item)
                .onNavigation {
                    listItemsVM.start(viewModel: item)
                }
        }
        .listStyle(.plain)
        .navigationTitle("Swift's Repo")
        .onAppear {
            self.listItemsVM.fetchingData()
        }
    }
}

struct ListItemsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ListItemsView(listItemsVM: ListItemsViewModel(coordinator: CoordinatorObject()))
            ListItemsView(listItemsVM: ListItemsViewModel(coordinator: CoordinatorObject()))
                .previewDevice("iPhone SE (2nd generation)")
        }
    }
}
