//
//  DetailItemsView.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import SwiftUI

struct DetailItemsView: View {
    
    @ScaledMetric var imageHeight: CGFloat = 150
    
    @ObservedObject var detailItemsVM: DetailItemsViewModel

    var body: some View {
        
        GeometryReader { geometry in
            
            ScrollView(showsIndicators: false) {
                
                VStack() {
                    
                    Spacer()
                    
                    AsyncImageView(url: detailItemsVM.viewModel.owner.avatarURL, placeholder: Text("Download...")).frame(height: imageHeight)
                    
                    Text(detailItemsVM.viewModel.name)
                        .font(.title)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.center)
                        .padding(.all)
                    
                    VStack(spacing: 10) {
                        Label("\(detailItemsVM.viewModel.watchers)", systemImage: "eye")
                        
                        Label("\(detailItemsVM.viewModel.forks)", systemImage: "tuningfork")
                        
                        Label("\(detailItemsVM.viewModel.stargazersCount)", systemImage: "star")
                    }
                    .foregroundColor(.secondary)
                    
                    Spacer()
                    
                    Text(detailItemsVM.viewModel.itemDescription ?? "")
                        .multilineTextAlignment(.center)
                        .padding(.all)
                    
                    Text(detailItemsVM.viewModel.htmlURL)
                        .font(.callout)
                        .padding(.horizontal)
                        .multilineTextAlignment(.center)
                        .foregroundColor(.blue)
                    
                    Text(detailItemsVM.viewModel.updatedAt)
                        .italic()
                        .multilineTextAlignment(.center)
                        .padding(.all)
                    
                    Spacer()
                }
                .frame(minHeight: geometry.size.height)
            }.frame(width: geometry.size.width)
        }
    }
}

struct DetailItemsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {

        }
    }
}
