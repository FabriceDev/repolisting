//
//  AsyncImageView.swift
//  RepoListing
//
//  Created by Fabrice M. on 06/03/2022.
//

import Foundation
import SwiftUI

struct AsyncImageView: View {
    
    private var imageURL: String
    private var imagePlaceholder: Text
    
    @ObservedObject var object = AsyncImageObject()
    
    init(url: String, placeholder: Text) {
        self.imageURL = url
        self.imagePlaceholder = placeholder
        self.object.load(imageURL: url)
    }
    
    var body: some View {
        VStack {
            if let image = object.image {
                Image(uiImage: image)
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(20)
            } else {
                imagePlaceholder
            }
        }
        .onAppear {  }
        .onDisappear {
            self.object.cancel()
        }
    }
}
