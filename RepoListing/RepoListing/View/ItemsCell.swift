//
//  ItemsCell.swift
//  RepoListing
//
//  Created by Fabrice M. on 06/03/2022.
//

import SwiftUI

struct ItemsCell: View {
    
    @ScaledMetric var imageHeight: CGFloat = 100
    
    var item: ItemsViewModel
    
    var body: some View {
        
        HStack(spacing: 10) {
            
            AsyncImageView(url: item.owner.avatarURL, placeholder: Text("Download...")).frame(height: imageHeight)
            
            VStack(alignment: .leading) {
                Text(item.name)
                    .font(.title2)
                    .foregroundColor(.red)
                
                Text(item.owner.login)
                    .font(.title3)
                    .fontWeight(.semibold)
                
                Spacer()
                Text(item.createdAt)
                    .foregroundColor(.secondary)
            }
        }
    }
}
