//
//  AsyncImageCache.swift
//  RepoListing
//
//  Created by Fabrice M. on 07/03/2022.
//

import Foundation
import UIKit

class AsyncImageCache {
    
    static let shared = AsyncImageCache()
    
    private var cache: NSCache = NSCache<NSString, UIImage>()
    
    subscript(key: String) -> UIImage? {
        get {
            cache.object(forKey: key as NSString)
        }
        set(image) {
            image == nil ? self.cache.removeObject(forKey: (key as NSString)) : self.cache.setObject(image!, forKey: (key as NSString))
        }
    }
    
}
