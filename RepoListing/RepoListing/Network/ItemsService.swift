//
//  ItemsService.swift
//  RepoListing
//
//  Created by Fabrice M. on 06/03/2022.
//

import Foundation
import Combine
import UIKit

protocol ItemsServiceProtocol {
    func fetchListItems() throws -> AnyPublisher<ListItems, Error>
}

class ItemsService: ItemsServiceProtocol {
    
    private let repoURL = "https://api.github.com/search/repositories?q=language:swift"
    
    func fetchListItems() throws -> AnyPublisher<ListItems, Error> {
        guard let url = URL(string: repoURL) else { throw ErrorService.urlConversion }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: ListItems.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
