//
//  AsyncImageObject.swift
//  RepoListing
//
//  Created by Fabrice M. on 06/03/2022.
//

import Foundation
import Combine
import UIKit

class AsyncImageObject: ObservableObject {
    
    @Published var image: UIImage?
    
    private var cancellable: AnyCancellable?
    
    private var cache = AsyncImageCache.shared
    
    func load(imageURL: String) {
        guard let url = URL(string: imageURL) else { return }
        
        if let image = cache[url.absoluteString] {
                self.image = image
                return
            }
        
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .handleEvents(receiveOutput: { self.cache[url.absoluteString] = $0 })
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }
    
    func cancel() {
        cancellable?.cancel()
    }
}
