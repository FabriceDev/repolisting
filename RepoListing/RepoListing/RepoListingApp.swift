//
//  RepoListingApp.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import SwiftUI

@main
struct RepoListingApp: App {
    var body: some Scene {
        WindowGroup {
            CoordinatorView(object: CoordinatorObject())
        }
    }
}
