//
//  CoordinatorObject.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import Foundation

class CoordinatorObject: ObservableObject {
    
    @Published var listItemsViewModel: ListItemsViewModel!
    @Published var detailItemsViewModel: DetailItemsViewModel?
    
    init() {
        self.listItemsViewModel = ListItemsViewModel(coordinator: self)
    }
    
    func start(viewModel: ItemsViewModel) {
        self.detailItemsViewModel = DetailItemsViewModel(viewModel: viewModel, coordinator: self)
    }
}
