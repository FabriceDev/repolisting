//
//  CoordinatorView.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import SwiftUI

struct CoordinatorView: View {
    
    @ObservedObject var object: CoordinatorObject
    
    var body: some View {
        NavigationView {
            ListItemsView(listItemsVM: object.listItemsViewModel)
                .navigation(item: $object.detailItemsViewModel) { DetailItemsView(detailItemsVM: $0) }
        }.navigationViewStyle(.stack)
    }
}

struct CoordinatorView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CoordinatorView(object: CoordinatorObject())
            CoordinatorView(object: CoordinatorObject())
                .previewDevice("iPhone 11 Pro Max")
                
        }
    }
}
