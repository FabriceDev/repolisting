//
//  Type+Extension.swift
//  RepoListing
//
//  Created by Fabrice M. on 07/03/2022.
//

import Foundation


extension String {
    
    func stringToDate() -> Date? {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        return formatter.date(from: self)
    }
}

extension Date {
    
    func dateToString() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale(identifier: "fr_FR")
        return formatter.string(from: self)
    }
}
