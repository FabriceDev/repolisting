//
//  Items.swift
//  RepoListing
//
//  Created by Fabrice M. on 04/03/2022.
//

import Foundation
import UIKit


// MARK: - ListItems
struct ListItems: Decodable {
    let items: [Item]

    enum CodingKeys: String, CodingKey {
        case items
    }
}

// MARK: - Item
struct Item: Decodable, Identifiable {
    let id: Int
    let name, fullName: String
    let itemPrivate: Bool
    let owner: Owner
    let htmlURL: String
    let itemDescription: String?
    let createdAt, updatedAt: String
    let stargazersCount: Int
    let license: License?
    let topics: [String]
    let forks, openIssues, watchers: Int
    let score: Int

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case itemPrivate = "private"
        case owner
        case htmlURL = "html_url"
        case itemDescription = "description"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case stargazersCount = "stargazers_count"
        case license
        case topics, forks
        case openIssues = "open_issues"
        case watchers
        case score
    }
}

// MARK: - License
struct License: Decodable {
    let name: String
}

// MARK: - Owner
struct Owner: Decodable {
    let login: String
    let id: Int
    let avatarURL: String

    enum CodingKeys: String, CodingKey {
        case login, id
        case avatarURL = "avatar_url"
    }
}

// MARK: - ErrorService
enum ErrorService: String, Error {
    case urlConversion = "Error: Can't convert String to URL type"
}


// MARK: - Mocked
let mockedItem = Item(id: 21700699, name: "awesome-ios", fullName: "vsouza/awesome-ios", itemPrivate: false, owner: mockedOwner, htmlURL: "https://github.com/vsouza/awesome-ios", itemDescription: "A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects", createdAt: "Date()", updatedAt: "Date()", stargazersCount: 23, license: mockedLicense, topics: [
    "apple-swift",
    "arkit",
    "awesome",
    "ios",
    "ios-animation",
    "ios-libraries",
    "objective-c",
    "objective-c-library",
    "swift-extensions",
    "swift-framework",
    "swift-language",
    "swift-library",
    "swift-programming"], forks: 7797, openIssues: 78, watchers: 6554, score: 2)

let mockedOwner = Owner(login: "vsouza", id: 24133, avatarURL: "Temp")
let mockedLicense = License(name: "MIT")
